Meteor.methods({
  seed() {
    Rating.insert({ createdAt: new Date(), rate: '1', createdAt: new Date() });
    Users.insert({ userType: 'customer', name: 'amr', userId: '1', createdAt: new Date() })
    Requests.insert({ userId: '1', requestId: '1', createdAt: new Date() })
    Orders.insert({ userId: '1', rate: '1', createdAt: new Date() })
  },
  getNewCustomers() {
    const startOfMonth = moment().startOf('month').format('YYYY-MM-DD hh:mm');
    const endOfMonth = moment().endOf('month').format('YYYY-MM-DD hh:mm');
    const usersCount = Users.find({ userType: 'customer', createdAt: { $gt: startOfMonth, $lt: endOfMonth } }).count();
    return usersCount;
  },
  getRequests() {
    const startOfMonth = moment().startOf('month').format('YYYY-MM-DD hh:mm');
    const endOfMonth = moment().endOf('month').format('YYYY-MM-DD hh:mm');
    const requestsCount = Requests.find({ createdAt: { $gt: startOfMonth, $lt: endOfMonth } }).count();
    return requestsCount;
  },
  getOrders() {
    const startOfMonth = moment().startOf('month').format('YYYY-MM-DD hh:mm');
    const endOfMonth = moment().endOf('month').format('YYYY-MM-DD hh:mm');
    const OrdersCount = Orders.find({ createdAt: { $gt: startOfMonth, $lt: endOfMonth } }).count();
    return OrdersCount;
  },
  getFulfilledOrders() {
    const startOfMonth = moment().startOf('month').format('YYYY-MM-DD hh:mm');
    const endOfMonth = moment().endOf('month').format('YYYY-MM-DD hh:mm');
    const fulfilledOrdersCount = Orders.find({ status: 'done', createdAt: { $gt: startOfMonth, $lt: endOfMonth } }).count();
    return fulfilledOrdersCount;
  },
  getRating() {
    const startOfMonth = moment().startOf('month').format('YYYY-MM-DD hh:mm');
    const endOfMonth = moment().endOf('month').format('YYYY-MM-DD hh:mm');
    const ratingCount = Rating.find({ createdAt: { $gt: startOfMonth, $lt: endOfMonth } }).count();
    return ratingCount;
  }
});
